const fs = require('fs');

const messages = './messages';

let data = [];
module.exports = {
    init() {
        try {
            const fileContent = fs.readdir(messages, (err, files) => {
                data = files.map(file => {
                    const message = fs.readFileSync(messages + '/' + file, 'utf8' );
                    return JSON.parse(message)
                })
            })

        } catch (e) {
            data = [];
        }
    },
    getItems(){
        return data.splice(data.length - 5)
    }

};