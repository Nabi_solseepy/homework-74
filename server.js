const express = require('express');
const app = express();
const messages = require('./messages');
const fileMs = require('./fileMs');


fileMs.init();

app.use(express.json());

const port = 8000;
app.use('/messages', messages);

app.listen(port);